window.onload = function()
{
  const tabObject = new Tabs('.tabs__tabs-list')
  const tabContentObject = new TabsContent('.tabs__content-list')
  tabObject.render()
  tabContentObject.render()
}