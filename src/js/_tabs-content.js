class TabsContent {
  constructor(parentSelector) {
    this.parent = document.querySelector(parentSelector)
  }

  data = [
    {
      id: 'tabcCont1',
      tabId: 'tab1',
      image: './dist/images/posts/post.png',
      title: 'Some Title Here 1',
      text: `
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.</p>
      <p>Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.</p>
      <p>Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.</p>
      `,
    },
    {
      id: 'tabcCont2',
      tabId: 'tab2',
      image: './dist/images/posts/post.png',
      title: 'Some Title Here 2',
      text: `
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.</p>
      <p>Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.</p>
      `,
    },
    {
      id: 'tabcCont3',
      tabId: 'tab3',
      image: './dist/images/posts/post.png',
      title: 'Some Title Here 3',
      text: `<p>Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.</p>`,
    },
  ]

  render = () => {
    if (this.parent) {
      const arr = this.data.map(this.template)
      this.parent.insertAdjacentHTML('afterbegin',arr.join(''))
    }
  }

  template = ({tabId,image,title,text},index) => `
  <li class="tabs__content-item ${index === 0 ? 'tabs-active' : '' }" data-tab-content="${tabId}">
    <div class="post">
      <div class="post__img">
        <img src="${image}" alt="${title}" />
      </div>
      <h4 class="post__title">${title}</h4>
      <div class="post__description">
       ${text}              
      </p>
    </div>
  </li>
  `
}
