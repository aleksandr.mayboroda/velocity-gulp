class Tabs {
  constructor(parentSelector) {
    this.parent = document.querySelector(parentSelector)
  }

  attr = {
    li: {
      className: 'tabs__tabs-item',
    },
    button: {
      className: 'btn-sm',
    },
    activeTab: {
      className: 'btn-active'
    },
    activeTabContent: {
      className: 'tabs-active'
    }

  }

  data = [
    { id: 'tab1', name: 'Tab Button 1' },
    { id: 'tab2', name: 'Tab Button 2' },
    { id: 'tab3', name: 'tab button 3' },
  ]

  render = () => {
    if (this.parent) {
      const tabs = this.data.map(this.template)
      this.parent.append(...tabs)
    }
  }

  template = ({ id, name }, index) => {
    const button = document.createElement('button')
    const li = document.createElement('li')
    button.className = this.attr.button.className
    if(index === 0) 
    {
      button.classList.add(this.attr.activeTab.className)
    }
    button.dataset.tabBtn = id
    button.innerText = name
    button.addEventListener('click',this.selectTab)
    li.className = this.attr.li.className
    li.append(button)
    return li
  }

 
  selectTab = (ev) => {
    const tab = ev.target
    if(!tab.dataset.tabBtn) return
    const id = tab.dataset.tabBtn
    if(!id) return
    const tabs = document.querySelectorAll('[data-tab-btn]')
    const tabContents = document.querySelectorAll('[data-tab-content]')

    if(tabs.length > 0 && tabContents.length > 0)
    {
      for(let i = 0; i < tabContents.length; i++)
      {
        tabContents[i].dataset.tabContent == id 
        ? tabContents[i].classList.add(this.attr.activeTabContent.className)
        : tabContents[i].classList.remove(this.attr.activeTabContent.className) //activeTabContent
      }
      for(let i = 0; i < tabs.length; i++)
      {
        tabs[i].dataset.tabBtn === id 
        ? tabs[i].classList.add(this.attr.activeTab.className)
        : tabs[i].classList.remove(this.attr.activeTab.className)
      }
    }
  }
}
