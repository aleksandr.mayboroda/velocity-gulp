// const gulp = require('gulp')
// const concat = require('gulp-concat')
// const clean = require('gulp-clean')
// const imagemin = require('gulp-imagemin')
// const browserSync = require('browser-sync').create()
// const sass = require('gulp-sass')(require('sass'))
// const uglify = require('gulp-uglify-es').default
// const autoprefixer = require('gulp-autoprefixer')
// const cleanCSS = require('gulp-clean-css')

import gulp from 'gulp'
import concat from 'gulp-concat'
import clean from 'gulp-clean'
import imagemin from 'gulp-imagemin'
import * as brSync from 'browser-sync'
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import ugly from 'gulp-uglify-es'
import autoprefixer from 'gulp-autoprefixer'
import cleanCSS from 'gulp-clean-css'

const browserSync = brSync.create();
const sass = gulpSass(dartSass);
const uglify = ugly.default;

const FOLDER = {
  dev: 'src/',
  prod: 'dist/',
}

const path = {
  src: {
    scss: `${FOLDER.dev}**/*.scss`,
    js: `${FOLDER.dev}**/*.js`,
    img: `${FOLDER.dev}images/**/*`,
  },
  build: {
    css: `${FOLDER.prod}css/`,
    js: `${FOLDER.prod}js/`,
    img: `${FOLDER.prod}images/`,
  },
}

const buildStyles = () =>
  gulp
    .src(path.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'] }))
    .pipe(cleanCSS())
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest(path.build.css))
    .pipe(browserSync.stream())

const buildJs = () =>
  gulp
    .src(path.src.js)
    .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest(path.build.js))
    .pipe(browserSync.stream())

const buildIMG = () =>
  gulp
    .src(path.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(path.build.img))
    .pipe(browserSync.stream())

//очистка папки + проверка на ее наличие
const cleanBuild = () =>
  gulp.src(`${FOLDER.prod}/`, { allowEmpty: true })
  .pipe(clean())

  //отслеживание измений в файлах
const watcher = () => {
  browserSync.init({
    server: {
      baseDir: './',
    },
  })

  gulp.watch(path.src.scss, buildStyles).on('change', browserSync.reload)
  gulp.watch(path.src.js, buildJs).on('change', browserSync.reload)
  gulp.watch(path.src.img, buildIMG).on('change', browserSync.reload)
  gulp.watch('./index.html', null).on('change', browserSync.reload)
}

gulp.task(
  'build',
  gulp.series(cleanBuild, gulp.parallel(buildIMG, buildJs, buildStyles))
)

gulp.task('dev', gulp.series('build', watcher))